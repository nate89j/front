import React from 'react';

import './App.css';
import Login from "./containers/Login";
import Home from "./containers/Home";
import { BrowserRouter, Route } from 'react-router-dom';



function App() {
  return (
    <div className="App">

      <BrowserRouter>
        <Route path="/" exact component={Login} />
        <Route path="/login" component={Login} />
        <Route path="/Home" component={Home} />


      </BrowserRouter>



    </div>
  );
}

export default App;
