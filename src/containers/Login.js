import { withRouter } from 'react-router-dom';
import React, { Component } from "react";
import { Button, FormGroup, FormControl } from "react-bootstrap";
import "./Login.css";


export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: ""

        };
        this.routeChange = this.routeChange.bind(this);
    }

    routeChange() {
        let path = `./Home`;
        this.props.history.push(path);
    }

    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();
    }



    render() {
        return (

            <div className="Login">
                <form onSubmit={this.handleSubmit}>
                    <div className="username" id="uname"> USERNAME
                        <FormGroup controlId="username" bsSize="large">
                            {/* //   <ControlLabel>Email</ControlLabel> */}

                            <FormControl
                                autoFocus
                                type="username"
                                value={this.state.username}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                    </div>
                    <div className="password" id="pass"> Pass
                        <FormGroup controlId="password" bsSize="large">
                            {/* <ControlLabel>Password</ControlLabel> */}

                            <FormControl
                                value={this.state.password}
                                onChange={this.handleChange}
                                type="password"
                            />
                        </FormGroup>
                    </div>
                    <Button id="button"
                        bsSize="large"
                        enabled={!this.validateForm()}
                        type="submit"
                        onClick={this.routeChange}
                    >
                        Login
          </Button>
                </form>
            </div>
        );
    }
}
